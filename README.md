# vuempit2020front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



### Build Docker image
```
docker build -t name/of/your/project .
```

### Run your Docker image
```
docker run -it -p 8080:8080 --rm --name name/of/docker/container name/of/your/project
```

