import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Items from '../views/Items.vue'
import FirstForm from '../views/FirstForm'
import SecondForm from '../views/SecondForm.vue'
import Forum from '../views/Forum.vue'
import Question from '../views/Question.vue'
import NewQuestion from "../views/NewQuestion.vue";
import Login from "../views/Login";




Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Items',
        component: Items
    },
    {
        path: '/first',
        name: 'FirstForm',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/FirstForm.vue')
    },
    {
        path: '/feed',
        name: 'Feed',
        component: Home
    },
    {
        path: '/second',
        name: 'SecondForm',
        component: SecondForm
    },
    {
        path: '/forum',
        name: 'Forum',
        component: Forum
    },
    {
        path: '/question/:id',
        name: 'Question',
        component: Question
    },
    {
        path: '/newquestion',
        name: 'NewQuestion',
        component: NewQuestion,

    },
    {
        path: '/login',
        name: 'Login',
        component: Login,

    }


]


const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
})



export default router
